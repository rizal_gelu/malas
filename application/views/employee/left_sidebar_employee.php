

            <div class="left side-menu">
                <div class="sidebar-inner slimscrollleft">
                    <!--- Divider -->
                    <div id="sidebar-menu">
                        <ul>

                            <li class="text-muted menu-title">Navigation</li>

                            <li class="has_sub">
                                <a href="<?php echo base_url()."client/" ?>" class="waves-effect"><i class="ti-home"></i> <span> Dashboard </span> <span class="menu-arrow"></span></a>
                            </li>

                            <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="ti-settings"></i> <span> Company </span> <span class="menu-arrow"></span> </a>
                                <ul class="list-unstyled">
                                    <li><a href="<?php echo base_url()."client/karyawan/" ?>"> Karyawan </a></li>
                                    <li><a href="<?php echo base_url()."client/department/" ?>"> Department </a></li>
                                    <li><a href="<?php echo base_url()."client/tunjangan/" ?>"> Tunjangan </a></li>
                                    <!-- <li><a href="ui-loading-buttons.html"> Gaji Karyawan </a></li> -->
                                    <li><a href="<?php echo base_url()."client/view_setting_company/" ?>"> Setting Company </a></li>
                                </ul>
                            </li>

                            <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="ti-files"></i> <span> Laporan </span> <span class="menu-arrow"></span> </a>
                                <ul class="list-unstyled">
                                    <li><a href="<?php echo base_url()."client/laporan_gaji/" ?>"> Laporan Gaji </a></li>
                                </ul>
                            </li>

                            <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="ti-shopping-cart"></i><span> Order </span> <span class="menu-arrow"></span> </a>
                                <ul class="list-unstyled">
                                    <li><a href="<?php echo base_url()."client/order/".$user->id ?>">View All Order</a></li>
                                </ul>
                            </li>

                            <li class="text-muted menu-title">Khusus</li>

                            <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="ti-light-bulb"></i><span> Pricing </span> <span class="menu-arrow"></span> </a>
                                <ul class="list-unstyled">
                                    <li><a href="<?php echo base_url()."client/pricing/" ?>">View Pricing</a></li>
                                </ul>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <!-- Left Sidebar End