<!DOCTYPE html>
<html>
    <!-- Start Head -->
      <?php echo $head; ?>
    <!-- End Head -->
    <body class="fixed-left">

        <!-- Begin page -->
        <div id="wrapper">

            <!-- Top Bar Start -->
              <?php echo $top_bar; ?>
            <!-- Top Bar End -->


            <!-- ========== Left Sidebar Start ========== -->
              <?php echo $left_sidebar; ?>
            <!-- Left Sidebar End --> 



            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->                      
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">

                        <!-- Page-Title -->
                        <div class="row">
                            <div class="col-sm-12">

                                <h4 class="page-title">Invoice</h4>
                                <ol class="breadcrumb">
                                    <li><a href="<?php echo base_url()."client/" ?>">Dashboard</a></li>
                                    <li class="active">Invoice</li>
                                </ol>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-default">
                                    <!-- <div class="panel-heading">
                                        <h4>Invoice</h4>
                                    </div> -->
                                    <div class="panel-body">
                                        <div class="clearfix">
                                            <div class="pull-left">
                                                <h4 class="text-right"><img src="assets/images/logo_dark.png" alt="velonic"></h4>
                                                
                                            </div>
                                            <div class="pull-right">
                                                <h4>Invoice # <br>
                                                    <strong><?php echo $data_invoice->code_order; ?></strong>
                                                </h4>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="row">
                                            <div class="col-md-12">
                                                
                                                <div class="pull-left m-t-30">
                                                    <address>
                                                      <strong>GelHosting - Absensi.</strong><br>
                                                      Jln. Margonda Raya, Gang Kapuk no.6D<br>
                                                      Depok - Jawa Barat 16424<br>
                                                      <abbr title="Phone">HP:</abbr> (+62) 812-8751-2559
                                                      </address>
                                                </div>
                                                <div class="pull-right m-t-30">
                                                    <p><strong>Order Date: </strong><?php echo date('d M Y', strtotime($data_invoice->date)); ?></p>

                                                    <!-- PERMISALAN JIKA STATUS -->
                                                    <?php if($data_invoice->status == 'Menunggu Pembayaran') { ?>
                                                        <p class="m-t-10"><strong>Order Status: </strong> <span class="label label-pink"><?php echo $data_invoice->status; ?></span></p>
                                                    <?php } else if($data_invoice->status == 'Sudah dibayar') { ?>
                                                        <p class="m-t-10"><strong>Order Status: </strong> <span class="label label-success"><?php echo $data_invoice->status; ?></span></p>
                                                    <?php } else if($data_invoice->status == 'Pending') { ?>
                                                        <p class="m-t-10"><strong>Order Status: </strong> <span class="label label-warning"><?php echo $data_invoice->status; ?></span></p>
                                                    <?php } else if($data_invoice->status == 'Cancel') { ?>
                                                        <p class="m-t-10"><strong>Order Status: </strong> <span class="label label-danger"><?php echo $data_invoice->status; ?></span></p>
                                                    <?php } ?>
                                                    <!-- END PERMISALAN STATUS -->

                                                    <p class="m-t-10"><strong>Order ID: </strong><?php echo $data_invoice->code_order; ?></p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="m-h-50"></div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="table-responsive">
                                                    <table class="table m-t-30">
                                                        <thead>
                                                            <tr><th>#</th>
                                                            <th>Item</th>
                                                            <th>Description</th>
                                                            <th>Quantity</th>
                                                            <th>Unit Cost</th>
                                                            <th>Total</th>
                                                        </tr></thead>
                                                        <tbody>
                                                            <tr>
                                                                <td>1</td>
                                                                <td><?php echo $data_invoice->title; ?></td>
                                                                <td>Max Employee : <?php echo $data_invoice->max_employee; ?> <br>
                                                                    Max Admin : <?php echo $data_invoice->max_admin; ?></td>
                                                                <td><?php echo $data_invoice->jumlah_bulan; ?> Bulan</td>
                                                                <td>Rp <?php echo number_format($data_invoice->total_harga / $data_invoice->jumlah_bulan); ?></td>
                                                                <td>Rp <?php echo number_format($data_invoice->total_harga); ?></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row" style="border-radius: 0px;">
                                            <div class="col-md-3 col-md-offset-9">
                                                <!-- <p class="text-right"><b>Sub-total:</b> 2930.00</p>
                                                <p class="text-right">Discout: 12.9%</p>
                                                <p class="text-right">VAT: 12.9%</p> -->
                                                <hr>
                                                <h3 class="text-right">Total : Rp <?php echo number_format($data_invoice->total_harga); ?></h3>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="hidden-print">
                                            <div class="pull-right">
                                                <a href="javascript:window.print()" class="btn btn-inverse waves-effect waves-light"><i class="fa fa-print"></i></a>
                                                <!-- <a href="#" class="btn btn-primary waves-effect waves-light">Submit</a> -->
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>

                    </div> <!-- container -->
                               
                </div> <!-- content -->

                <footer class="footer">
                    © 2016. All rights reserved.
                </footer>

            </div>
            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->


            <!-- Right Sidebar -->
            <div class="side-bar right-bar nicescroll">
                <h4 class="text-center">Chat</h4>
                <div class="contact-list nicescroll">
                    <ul class="list-group contacts-list">
                        <li class="list-group-item">
                            <a href="#">
                                <div class="avatar">
                                    <img src="assets/images/users/avatar-1.jpg" alt="">
                                </div>
                                <span class="name">Chadengle</span>
                                <i class="fa fa-circle online"></i>
                            </a>
                            <span class="clearfix"></span>
                        </li>
                        <li class="list-group-item">
                            <a href="#">
                                <div class="avatar">
                                    <img src="assets/images/users/avatar-2.jpg" alt="">
                                </div>
                                <span class="name">Tomaslau</span>
                                <i class="fa fa-circle online"></i>
                            </a>
                            <span class="clearfix"></span>
                        </li>
                        <li class="list-group-item">
                            <a href="#">
                                <div class="avatar">
                                    <img src="assets/images/users/avatar-3.jpg" alt="">
                                </div>
                                <span class="name">Stillnotdavid</span>
                                <i class="fa fa-circle online"></i>
                            </a>
                            <span class="clearfix"></span>
                        </li>
                        <li class="list-group-item">
                            <a href="#">
                                <div class="avatar">
                                    <img src="assets/images/users/avatar-4.jpg" alt="">
                                </div>
                                <span class="name">Kurafire</span>
                                <i class="fa fa-circle online"></i>
                            </a>
                            <span class="clearfix"></span>
                        </li>
                        <li class="list-group-item">
                            <a href="#">
                                <div class="avatar">
                                    <img src="assets/images/users/avatar-5.jpg" alt="">
                                </div>
                                <span class="name">Shahedk</span>
                                <i class="fa fa-circle away"></i>
                            </a>
                            <span class="clearfix"></span>
                        </li>
                        <li class="list-group-item">
                            <a href="#">
                                <div class="avatar">
                                    <img src="assets/images/users/avatar-6.jpg" alt="">
                                </div>
                                <span class="name">Adhamdannaway</span>
                                <i class="fa fa-circle away"></i>
                            </a>
                            <span class="clearfix"></span>
                        </li>
                        <li class="list-group-item">
                            <a href="#">
                                <div class="avatar">
                                    <img src="assets/images/users/avatar-7.jpg" alt="">
                                </div>
                                <span class="name">Ok</span>
                                <i class="fa fa-circle away"></i>
                            </a>
                            <span class="clearfix"></span>
                        </li>
                        <li class="list-group-item">
                            <a href="#">
                                <div class="avatar">
                                    <img src="assets/images/users/avatar-8.jpg" alt="">
                                </div>
                                <span class="name">Arashasghari</span>
                                <i class="fa fa-circle offline"></i>
                            </a>
                            <span class="clearfix"></span>
                        </li>
                        <li class="list-group-item">
                            <a href="#">
                                <div class="avatar">
                                    <img src="assets/images/users/avatar-9.jpg" alt="">
                                </div>
                                <span class="name">Joshaustin</span>
                                <i class="fa fa-circle offline"></i>
                            </a>
                            <span class="clearfix"></span>
                        </li>
                        <li class="list-group-item">
                            <a href="#">
                                <div class="avatar">
                                    <img src="assets/images/users/avatar-10.jpg" alt="">
                                </div>
                                <span class="name">Sortino</span>
                                <i class="fa fa-circle offline"></i>
                            </a>
                            <span class="clearfix"></span>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- /Right-bar -->


        </div>
        <!-- END wrapper -->

        <!-- Start Javascript -->
          <?php echo $javascript; ?>
        <!-- End Javascript -->
	</body>
</html>