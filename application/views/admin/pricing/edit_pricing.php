<!DOCTYPE html>
<html>
<!-- Start Head -->
  <?php echo $head; ?>
<!-- End Head -->

<body class="fixed-left">

<!-- Begin page -->
<div id="wrapper">

            <!-- Top Bar Start -->
              <?php echo $top_bar; ?>
            <!-- Top Bar End -->


            <!-- Left Sidebar Start -->
              <?php echo $left_sidebar; ?>
            <!-- Left Sidebar End -->

    <!-- ============================================================== -->
    <!-- Start right Content here -->
    <!-- ============================================================== -->
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container">

                <!-- Page-Title -->
                <div class="row">
                            <div class="col-sm-12">
                                <div class="card-box">
                                    <h4 class="m-t-0 header-title"><b>Bootstrap MaxLength</b></h4>
                                    
                                    <div class="row">
                                    <form method="post" action="<?php echo base_url()."admin/edit_pricing_proses" ?>">
                                        <div class="col-md-6">
                                            <div class="p-20">
                                                <input type="hidden" name="id_pricing" value="<?php echo $edit_pricing->id_pricing; ?>">
                                                <h5><b>Title</b></h5>
                                                <input type="text" class="form-control" maxlength="25" name="title" id="defaultconfig" value="<?php echo $edit_pricing->title; ?>" />
                                                
                                                <div class="m-t-20">
                                                    <h5><b>Max Employee</b></h5>
                                                    <input type="text" maxlength="25" name="max_employee" class="form-control" id="thresholdconfig" value="<?php echo $edit_pricing->max_employee; ?>" />
                                                </div>
                                                
                                                <div class="m-t-20">
                                                    <h5><b>Max Admin</b></h5>
                                                    <input type="text" class="form-control" maxlength="25" name="max_admin" id="moreoptions" value="<?php echo $edit_pricing->max_admin; ?>" />
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="col-md-6">
                                            <div class="p-20">
                                                <h5><b>Price</b></h5>
                                                <input type="text" class="form-control" maxlength="25" name="price" id="alloptions" value="<?php echo $edit_pricing->price; ?>" />
                                                
                                                <div class="m-t-20">
                                                    <h5><b>Total Days</b></h5>
                                                    <input type="text" class="form-control" maxlength="25" name="days" id="placement" value="<?php echo $edit_pricing->days; ?>" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group text-right m-t-20 m-b-0">
                                            <div class="col-xs-12">
                                                <button class="btn btn-pink text-uppercase waves-effect waves-light w-sm" type="submit" id="btnSubmit">
                                                  Create Pricing
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                <!-- end row -->
              


            </div> <!-- container -->

        </div> <!-- content -->

        <footer class="footer">
            © 2016. All rights reserved.
        </footer>

    </div>
    <!-- ============================================================== -->
    <!-- End Right content here -->
    <!-- ============================================================== -->


    <!-- Right Sidebar -->
    <div class="side-bar right-bar nicescroll">
        <h4 class="text-center">Chat</h4>
        <div class="contact-list nicescroll">
            <ul class="list-group contacts-list">
                <li class="list-group-item">
                    <a href="#">
                        <div class="avatar">
                            <img src="assets/images/users/avatar-1.jpg" alt="">
                        </div>
                        <span class="name">Chadengle</span>
                        <i class="fa fa-circle online"></i>
                    </a>
                    <span class="clearfix"></span>
                </li>
                <li class="list-group-item">
                    <a href="#">
                        <div class="avatar">
                            <img src="assets/images/users/avatar-2.jpg" alt="">
                        </div>
                        <span class="name">Tomaslau</span>
                        <i class="fa fa-circle online"></i>
                    </a>
                    <span class="clearfix"></span>
                </li>
                <li class="list-group-item">
                    <a href="#">
                        <div class="avatar">
                            <img src="assets/images/users/avatar-3.jpg" alt="">
                        </div>
                        <span class="name">Stillnotdavid</span>
                        <i class="fa fa-circle online"></i>
                    </a>
                    <span class="clearfix"></span>
                </li>
                <li class="list-group-item">
                    <a href="#">
                        <div class="avatar">
                            <img src="assets/images/users/avatar-4.jpg" alt="">
                        </div>
                        <span class="name">Kurafire</span>
                        <i class="fa fa-circle online"></i>
                    </a>
                    <span class="clearfix"></span>
                </li>
                <li class="list-group-item">
                    <a href="#">
                        <div class="avatar">
                            <img src="assets/images/users/avatar-5.jpg" alt="">
                        </div>
                        <span class="name">Shahedk</span>
                        <i class="fa fa-circle away"></i>
                    </a>
                    <span class="clearfix"></span>
                </li>
                <li class="list-group-item">
                    <a href="#">
                        <div class="avatar">
                            <img src="assets/images/users/avatar-6.jpg" alt="">
                        </div>
                        <span class="name">Adhamdannaway</span>
                        <i class="fa fa-circle away"></i>
                    </a>
                    <span class="clearfix"></span>
                </li>
                <li class="list-group-item">
                    <a href="#">
                        <div class="avatar">
                            <img src="assets/images/users/avatar-7.jpg" alt="">
                        </div>
                        <span class="name">Ok</span>
                        <i class="fa fa-circle away"></i>
                    </a>
                    <span class="clearfix"></span>
                </li>
                <li class="list-group-item">
                    <a href="#">
                        <div class="avatar">
                            <img src="assets/images/users/avatar-8.jpg" alt="">
                        </div>
                        <span class="name">Arashasghari</span>
                        <i class="fa fa-circle offline"></i>
                    </a>
                    <span class="clearfix"></span>
                </li>
                <li class="list-group-item">
                    <a href="#">
                        <div class="avatar">
                            <img src="assets/images/users/avatar-9.jpg" alt="">
                        </div>
                        <span class="name">Joshaustin</span>
                        <i class="fa fa-circle offline"></i>
                    </a>
                    <span class="clearfix"></span>
                </li>
                <li class="list-group-item">
                    <a href="#">
                        <div class="avatar">
                            <img src="assets/images/users/avatar-10.jpg" alt="">
                        </div>
                        <span class="name">Sortino</span>
                        <i class="fa fa-circle offline"></i>
                    </a>
                    <span class="clearfix"></span>
                </li>
            </ul>
        </div>
    </div>
    <!-- /Right-bar -->


</div>
<!-- END wrapper -->

<!-- Start Javascript -->
  <?php echo $javascript; ?>
<!-- End Javascript -->

</body>
</html>