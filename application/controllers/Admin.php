<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	function __construct() 
    {
        parent::__construct();
 		$this->load->helper('url');
		
		$this->load->library('ion_auth');
		$this->load->library('uuid');
		$this->load->library('session');
		$this->load->library('form_validation');
		$this->auth = new stdClass;

		$this->load->model('admin_model');
	}

	function index()
	{
		if($this->ion_auth->is_admin())
		{
			$comp = array(
				'head'			=> $this->head(),
				'left_sidebar'	=> $this->left_sidebar(),
				'top_bar'		=> $this->top_bar(),
				'javascript'	=> $this->javascript()
			);
			$this->load->view('admin/dashboard/dashboard', $comp);
		}
		else
		{
			$this->session->set_flashdata('message', 'If you have access here, please login');
			redirect('login_admin');
		}
	}

	function post_pricing()
	{
		if($this->ion_auth->is_admin())
		{
			$comp = array(
				'head'			=> $this->head(),
				'left_sidebar'	=> $this->left_sidebar(),
				'top_bar'		=> $this->top_bar(),
				'javascript'	=> $this->javascript()
			);
			$this->load->view('admin/pricing/post_pricing', $comp);
		}
		else
		{
			$this->session->set_flashdata('message', 'If you have access here, please login');
			redirect('login_admin');
		}
	}

	function post_pricing_proses()
	{
		if($this->ion_auth->is_admin())
		{
			$user = $this->ion_auth->user()->row();
			$data = array(
				'id_pricing'	=> $this->uuid->v5(date('Y-m-d H:i:s')),
				'title'			=> $this->input->post('title'),
				'max_employee'	=> $this->input->post('max_employee'),
				'max_admin'		=> $this->input->post('max_admin'),
				'price'			=> $this->input->post('price'),
				'days'			=> $this->input->post('days'),
				'created_by'	=> $user->username,
				'date_created'	=> date('Y-m-d H:i:s')
			);

			$syg = $this->admin_model->input_pricing($data);
			if($syg)
			{
				redirect('admin/view_pricing');
			}
			else
			{
				redirect('admin/post_pricing');
			}
		}
		else
		{
			$this->session->set_flashdata('message', 'If you have access here, please login');
			redirect('login_admin');
		}
	}

	function view_pricing()
	{
		if($this->ion_auth->is_admin())
		{
			$comp = array(
				'get_pricing'	=> $this->admin_model->get_pricing(),
				'head'			=> $this->head(),
				'left_sidebar'	=> $this->left_sidebar(),
				'top_bar'		=> $this->top_bar(),
				'javascript'	=> $this->javascript()
			);
			$this->load->view('admin/pricing/view_pricing', $comp);
		}
		else
		{
			$this->session->set_flashdata('message', 'If you have access here, please login');
			redirect('login_admin');
		}
	}

	function edit_pricing()
	{
		if($this->ion_auth->is_admin())
		{
			if ($this->uri->segment(2) === False)
			{
				$id_pricing = 0;
			}
			else
			{
				$id_pricing = $this->uri->segment(3);
			}
			$comp = array(
				'edit_pricing'	=> $this->admin_model->get_pricing_by_id($id_pricing),
				'head'			=> $this->head(),
				'left_sidebar'	=> $this->left_sidebar(),
				'top_bar'		=> $this->top_bar(),
				'javascript'	=> $this->javascript()
			);
			$this->load->view('admin/pricing/edit_pricing', $comp);
		}
		else
		{
			$this->session->set_flashdata('message', 'If you have access here, please login');
			redirect('login_admin');
		}
	}

	function edit_pricing_proses()
	{
		if($this->ion_auth->is_admin())
		{
			$user = $this->ion_auth->user()->row();
			$id_pricing = $this->input->post('id_pricing');
			$data = array(
				'title'			=> $this->input->post('title'),
				'max_employee'	=> $this->input->post('max_employee'),
				'max_admin'		=> $this->input->post('max_admin'),
				'price'			=> $this->input->post('price'),
				'days'			=> $this->input->post('days'),
				'created_by'	=> $user->username,
			);

			$syg = $this->admin_model->update_pricing($id_pricing, $data);
			if($syg)
			{
				redirect('admin/view_pricing');
			}
			else
			{
				redirect('admin/edit_pricing');
			}
		}
		else
		{
			$this->session->set_flashdata('message', 'If you have access here, please login');
			redirect('login_admin');
		}
	}

	function head()
	{
		$data = array();
		return $this->load->view('admin/head', $data, true);
	}

	function left_sidebar()
	{
		$data = array();
		return $this->load->view('admin/left_sidebar', $data, true);
	}

	function top_bar()
	{
		$data = array();
		return $this->load->view('admin/top_bar', $data, true);
	}

	function javascript()
	{
		$data = array();
		return $this->load->view('admin/javascript', $data, true);
	}
}
