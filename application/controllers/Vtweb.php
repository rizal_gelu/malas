<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Vtweb extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */


	public function __construct()
    {
        parent::__construct();
        $params = array('server_key' => 'VT-server-vSiewVyK2i8kNPEUAc7dHMY2', 'sandbox' => false);
		$this->load->library('veritrans');
		$this->veritrans->config($params);
		$this->load->helper('url');

		$this->load->library('cart');
		$this->load->library('ion_auth');
		$this->load->library('uuid');

		$this->load->model('admin_model');
		
    }

	public function index()
	{
		$this->load->view('veritrans/checkout_vtweb');
	}

	public function vtweb_checkout()
	{
		// Last update cart, sebelum masuk ke pembayaran
		$no = 1;
		$data = array(
			'rowid' => $this->input->post($no.'[rowid]'),
			'qty'	=> $this->input->post('qty') / 30
		);

		$this->cart->update($data);

		$total = $this->cart->total();
		
		$transaction_details = array(
			'order_id' 			=> "ORD-" . date('YmdHis') . rand(0, 10000), //$data['id'], code_order
			'gross_amount' 		=> $total
		);

		//Get Information Users
		$user = $this->ion_auth->user()->row();

		// Populate customer's billing address
		$billing_address = array(
			'first_name' 		=> $user->first_name,
			'last_name' 		=> $user->last_name,
			'phone' 			=> $user->phone,
			'country_code'		=> 'IDN'
			);

		// Populate customer's shipping address
		$shipping_address = array(
			'first_name' 		=> $user->first_name,
			'last_name' 		=> $user->last_name,
			'phone' 			=> $user->phone,
			'country_code'		=> 'IDN'
			);

		// Populate customer's Info
		$customer_details = array(
			'first_name' 		=> $user->first_name,
			'last_name' 		=> $user->last_name,
			'email' 			=> $user->email,
			'phone' 			=> $user->phone,
			'billing_address' 	=> $billing_address,
			'shipping_address'	=> $shipping_address
			);

		//saring id pricing
		foreach($this->cart->contents() as $d)
		{
			$data_pricing = array(
				'id_pricing' 	=> $d['id'],
				'jumlah_bulan'	=> $d['qty']
			);
		}

		// input name company to table setting company
		$id_company = $this->uuid->v5(date('Y-m-d H:i:s'));
		$data_company = array(
			'company_id'	=> $id_company,
			'company_name'	=> $this->input->post('company_name')
		);
		$this->admin_model->input_setting_company($data_company);

		// Input user to employee
		$qr_code 		= $this->uuid->v5(date('Y-m-d H:i:s'));
		$data_employee 	= array(
	        'name'			=> $user->first_name ."". $user->last_name,
	       	'employee_id'	=> $qr_code,
	       	'id_user'		=> $user->id,
	       	'company_id'	=> $id_company,
	       	'create_date'	=> date('Y-m-d H:i:s'),
	    );
	    $this->admin_model->input_employee($data_employee);

		// Post Data to Table Pesanan
		$pesanan_id 	= $this->uuid->v5(date('Y-m-d H:i:s'));
		$data = array(
			'pesanan_id'		=> $pesanan_id,
			'code_order'		=> $transaction_details['order_id'],
			'user_id'			=> $user->id, 
			'id_pricing'		=> $data_pricing['id_pricing'],
			'nama_pembeli'		=> $user->first_name . " " . $user->last_name,
			'company_id'		=> $id_company,
			'company_name'		=> $data_company['company_name'],
			'handphone_pembeli'	=> $user->phone,
			'email_pembeli'		=> $user->email,
			'jumlah_bulan'		=> $data_pricing['jumlah_bulan'],
			'total_harga'		=> $total,
			'date'				=> date('Y-m-d H:i:s')
			);

	    $syg = $this->admin_model->insert_pesanan($data);
		
		$items = [];
		$i = 0;
		foreach ($this->cart->contents() as $item)
		{
			$items[$i] = array(
					//'id'		=> $item['rowid'],
					'id'		=> $item['id'],
					'quantity'  => $item['qty'],
				    'price'   	=> $item['price'],
					'name'    	=> (strlen($item['name']) > 40) ? substr($item['name'],0,40).'...' : $item['name'] 
			);
			$i++;
		}

		// Data yang akan dikirim untuk request redirect_url.
		// Uncomment 'credit_card_3d_secure' => true jika transaksi ingin diproses dengan 3DSecure.
		$transaction_data = array(
			'payment_type' 			=> 'vtweb', 
			'vtweb' 				=> array(
			//'enabled_payments' 	=> ['credit_card'],
			'credit_card_3d_secure' => true
			),
			'transaction_details'   => $transaction_details,
			'item_details' 			=> $items,
			'customer_details' 	    => $customer_details
		);

		try
		{
			$vtweb_url = $this->veritrans->vtweb_charge($transaction_data);
			header('Location: ' . $vtweb_url);	
		} 
		catch (Exception $e) 
		{
    		echo $e->getMessage();	
		}	
	}
}
