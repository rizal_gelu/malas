<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Client extends CI_Controller {

	function __construct() 
    {
        parent::__construct();
 		$this->load->helper('url');
		
		$this->load->library('ion_auth');
		$this->load->library('uuid');
		$this->load->library('ciqrcode');
		$this->load->library('session');
		$this->load->library('cart');
		$this->load->library('form_validation');
		$this->load->library('email');
		$this->auth = new stdClass;

		$this->load->model('admin_model');
		$this->load->helper('date');
		$this->load->helper('email');
	}

	function index()
	{
		if($this->ion_auth->logged_in() != $this->ion_auth->is_admin())
		{
			$comp = array(
				'head'			=> $this->head(),
				'left_sidebar'	=> $this->left_sidebar(),
				'top_bar'		=> $this->top_bar(),
				'javascript'	=> $this->javascript()
			);
			$this->load->view('client/dashboard/dashboard', $comp);
		}
		else
		{
			redirect('login_client');
		}
	}

	function barang() {
		if($this->ion_auth->logged_in() != $this->ion_auth->is_admin())
		{
			$comp = array(
				'head' 			=> $this->head(),
				'left_sidebar'	=> $this->left_sidebar(),
				'top_bar'		=> $this->top_bar(),
				'javascript'	=> $this->javascript(),
			);
			$this->load->view('client/barang/barang', $comp);
		}
	}
	
	function head()
	{
		$data = array(
			
		);
		return $this->load->view('client/head', $data, true);
	}

	function left_sidebar()
	{
		$data = array(
			'user'	=> $this->ion_auth->user()->row()
		);
		return $this->load->view('client/left_sidebar', $data, true);
	}

	function top_bar()
	{
		$data = array(
			'user' => $this->ion_auth->user()->row()
		);
		return $this->load->view('client/top_bar', $data, true);
	}

	function javascript()
	{
		$data = array();
		return $this->load->view('client/javascript', $data, true);
	}
}